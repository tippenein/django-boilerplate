from django.contrib.auth            import logout
from django.shortcuts               import render_to_response
from django.http                    import HttpResponseRedirect
from django.template                import RequestContext
from django.core.urlresolvers       import reverse
from django.contrib                 import messages
from django.utils.translation       import ugettext_lazy as _


def login(request, user):

  if user.is_authenticated():
    return render_to_response('site/index.html')

def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/')
